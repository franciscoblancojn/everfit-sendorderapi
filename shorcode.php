<?php
function saveGetPost( $atts, $content = null ) {
    $id = get_the_ID();
    update_post_meta($id,"GET",json_encode($_GET));
    update_post_meta($id,"POST",json_encode($_POST));

    if(isset($_GET['x_id_factura'])){
        $order_id = $_GET['x_id_factura'];

        update_post_meta($order_id,'x_ref_payco',$_GET['x_ref_payco']);
        $api = new api_send_order_processing();
        $api->SendOrderProcessing($order_id);
    }
    ?>
    <script>
        window.location = '/'
    </script>
    <?php
}
add_shortcode( 'saveGetPost', 'saveGetPost' );