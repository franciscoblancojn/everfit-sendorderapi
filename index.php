<?php
/*
Plugin Name: SendOrderApi
Plugin URI: https://startscoinc.com/es/
Description: envia informacion al api despues de que la order pase a procesando
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/

/**
 * Require api
 * Class for request
 */
require_once plugin_dir_path( __FILE__ )."api.php";
/**
 * Require Hook
 * Action for change order
 */
require_once plugin_dir_path( __FILE__ )."hook.php";
/**
 * Require Checkout
 * Add custom field in Checkout for api->descuentoNomina
 */
require_once plugin_dir_path( __FILE__ )."checkout.php";
/**
 * Require Shorcode
 * Add shorcode for get url epayco
 */
require_once plugin_dir_path( __FILE__ )."shorcode.php";