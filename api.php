<?php
if(!class_exists("api_send_order_processing")){
    class api_send_order_processing{

        private $URL_API = "https://b2cpedidos.everfituniforms.com";
        private $token = "";
        public function __construct($settings = null)
        {
            
        }
        public function request($json , $url , $type = "POST")
        {
            $header = array(
                "Content-Type: application/json"
            );
            if($this->token != ""){
                $header[] = 'Authorization: Bearer '.$this->token;
            }
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $type,
                CURLOPT_POSTFIELDS => $json,
                CURLOPT_HTTPHEADER => $header,
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            return ($response);
        }
        public function login()
        {
            $json_send = '{
                "usuario": "usr1",
                "password": "usr1"
            }';
            $r = $this->request($json_send , $this->URL_API."/api/Login");
            $r = json_decode($r,true);
            $this->token = $r['token'];
        }
        public function SendOrderProcessing($order_id)
        {
            $order = wc_get_order( $order_id );
            $user_id = $order->get_user_id();
            $user = get_user_by('id', $user_id);
            $user_data = get_userdata($user_id);
            
            $detalleOrden = [];
            foreach ( $order->get_items() as $item_id => $item ) {
                $product_id = $item->get_product_id();
                $variation_id = $item->get_variation_id();
                if($variation_id!=0){
                    $product_id = $variation_id;
                }
                $product = wc_get_product( $product_id );
                $quantity = $item->get_quantity();

                $detalleOrden[]= array(
                    "sku" => $product->get_sku(),
                    "cantidad" => $quantity,
                    "valorUnitario" => floatval($product->get_price())
                );
            }
            $json_send = array(
                "id_Orden"=> intval($order_id),
                "fecha"=> date_format($order->get_date_created(), "c"),
                "documento"=>  get_post_meta( $order_id, '_cedula', true ),
                "nombre"=> $order->get_billing_first_name().$order->get_billing_last_name(),
                "email"=> $order->get_billing_email(),
                "telefono"=> $order->get_billing_phone(),
                "departamento"=> $order->get_billing_state(),
                "ciudad"=> $order->get_billing_city(),
                "direccion"=> $order->get_billing_address_1(),
                "refEpayco"=> get_post_meta($order_id,'x_ref_payco',true),
                "estado"=> 2,
                "valorTotal_Flete"=> floatval($order->get_total()),
                "descuento"=> floatval($order->get_total_discount()),
                "codigoDescuento"=> implode($order->get_coupon_codes(),','),
                "descuentoNomina"=> "no",
                "cliente"=> implode($user_meta->roles,","),
                "detalleOrden"=> $detalleOrden
            );
            $json_send = json_encode($json_send);
            update_post_meta($order_id,'send',$json_send);
            $this->login();
            $r = $this->request($json_send , $this->URL_API."/api/UserOrder");
            update_post_meta($order_id,'order_api',json_encode($r));
            return $r;
        }
        public function GetEmpleadoById($id)
        {
            return $this->request("" , $this->URL_API."/api/Empleado/".$id , "GET");
        }
    }
}