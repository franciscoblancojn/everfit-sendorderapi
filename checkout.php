<?php
//cedula
//add
add_action( 'woocommerce_before_order_notes', 'add_cedula_checkout' , 10, 1);  
function add_cedula_checkout( $checkout ) { 
   $current_user = wp_get_current_user();
   $cedula = $current_user->cedula;
   woocommerce_form_field( 'cedula', array(        
      'type' => 'text',               
      'label' => __('Cedula'),     
      'required' => true,        
      'default' => $cedula,        
   ), $checkout->get_value( 'cedula' ) ); 
}
//valiadate
add_action( 'woocommerce_checkout_process', 'validate_cedula_checkout' , 10, 1);
function validate_cedula_checkout() {    
    if ( ! $_POST['cedula'] ) {
        wc_add_notice( 'Por favor ingresa tu Cedula  ', 'error' );
        return;
    }
}
//save
add_action( 'woocommerce_checkout_update_order_meta', 'save_cedula_checkout' , 10, 1);
function save_cedula_checkout( $order_id ) { 
    if ( $_POST['cedula'] ) update_post_meta( $order_id, '_cedula', esc_attr( $_POST['cedula'] ) );
}
//show
add_action( 'woocommerce_admin_order_data_after_billing_address', 'show_cedula_checkout', 10, 1 );
function show_cedula_checkout( $order ) {    
   $order_id = $order->get_id();
   if ( get_post_meta( $order_id, '_cedula', true ) ) echo '<p><strong>Cedula:</strong> ' . get_post_meta( $order_id, '_cedula', true ) . '</p>';
}
//descuentoNomina
//add
add_action( 'woocommerce_before_order_notes', 'add_descuentoNomina' , 10, 1);  
function add_descuentoNomina( $checkout ) { 
   $current_user = wp_get_current_user();
   $descuentoNomina = $current_user->descuentoNomina;
   woocommerce_form_field( 'descuentoNomina', array(        
      'type' => 'checkbox',               
      'label' => __('descuentoNomina'),       
      'default' => $descuentoNomina,        
   ), $checkout->get_value( 'descuentoNomina' ) ); 
}
//valiadate
add_action( 'woocommerce_checkout_process', 'validate_descuentoNomina_checkout' , 10, 1);
function validate_descuentoNomina_checkout() {  
    if( "1" == $_POST['descuentoNomina']){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://b2cpedidos.everfituniforms.com/api/Empleado/'.$_POST['cedula'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDYyNDg1NDUsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0OjU3OTAwIiwiYXVkIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NTc5MDAifQ.T6HQyEshARMiy0hefnhmcd5bYoh96X4rJni_eDLvZeA',
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        if($response == "false"){
            wc_add_notice( "Tu Cedula no permite DescuentoNomina", 'error' );
        }
    }
}
//save
add_action( 'woocommerce_checkout_update_order_meta', 'save_descuentoNomina_checkout' , 10, 1);
function save_descuentoNomina_checkout( $order_id ) { 
    if ( $_POST['descuentoNomina'] ) update_post_meta( $order_id, '_descuentoNomina', esc_attr( $_POST['descuentoNomina'] ) );
}
//show
add_action( 'woocommerce_admin_order_data_after_billing_address', 'show_descuentoNomina_checkout', 10, 1 );
function show_descuentoNomina_checkout( $order ) {    
   $order_id = $order->get_id();
   if ( get_post_meta( $order_id, '_descuentoNomina', true ) ) echo '<p><strong>DescuentoNomina:</strong> ' . get_post_meta( $order_id, '_descuentoNomina', true ) . '</p>';
}


function hook_checkout_footer()
{
    if(is_checkout()){
        ?>
        <style>
            #descuentoNomina_field,
            #descuentoNomina_field .optional{ 
                display:none;
                text-transform:uppercase;
            }
            #descuentoNomina_field .optional{ 
                display:none;
            }
        </style>
        <script>
            const url = "https://b2cpedidos.everfituniforms.com/api/Empleado/"
            cedula = document.getElementById('cedula')
            descuentoNomina_field = document.getElementById('descuentoNomina_field')
            if(cedula != null && cedula != undefined){
                cedula.onchange = function(){
                    value = cedula.value 
                    console.log(value);
                    request(value)
                }
            }
            getOptions= () =>{
                var myHeaders = new Headers();
                myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDYyNDg1NDUsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0OjU3OTAwIiwiYXVkIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NTc5MDAifQ.T6HQyEshARMiy0hefnhmcd5bYoh96X4rJni_eDLvZeA");
                //myHeaders.append("Cookie", "__cfduid=d21e181054ddbe141ad94be3b72cc92d21606242385");

                return {
                    method: 'GET',
                    headers: myHeaders,
                    redirect: 'follow'
                }
            }
            request = (id) =>{
                fetch(url + id, getOptions())
                    .then(response => response.text())
                    .then(result => {
                        console.log(result);
                        if(result === "true"){
                            descuentoNomina_field.style.display="block"
                            alert("Desea que la compra sea Descuento de Nomina?")
                        }else{
                            descuentoNomina_field.style.display="none"
                        }
                    })
                    .catch(error => {
                        console.log('error', error)
                    });
            }
        </script>
        <?php
    }
}

add_action( 'wp_footer', 'hook_checkout_footer' );